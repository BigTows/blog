# Blog - self made
##### Project in less than 24 hours

# Requirements
* PHP 7.4+
* MySQL 8.0+
* WebServer or something (Nginx, Local php server, apache etc..)

#Installation
1) Just create Database application will create all tables automatic or apply `dump.sql` from `meta` folder
1) Edit database configuration `src/configuration/di.php:22`
1) Run


#External links
* Background from [codepen](https://codepen.io/mohaiman/pen/MQqMyo)