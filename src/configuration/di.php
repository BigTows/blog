<?php

use Blog\Component\DataBase\DataBaseConfiguration;
use Blog\Component\DataBase\DataBaseConnection;
use Blog\Component\DataBase\DataBaseConnectionPdo;
use Blog\Component\TemplateManager\TemplateManager;
use Blog\Component\TemplateManager\TemplateManagerImpl;
use Blog\Controller\Api\HistoryRestController;
use Blog\Controller\HistoryController;
use Blog\Controller\IndexController;
use Blog\Controller\LoginController;
use Blog\Controller\LogoutController;
use Blog\Controller\RegisterController;
use Blog\Controller\TestController;
use Blog\Repository\HistoryRepository;
use Blog\Repository\HistoryRepositoryImpl;
use Blog\Repository\UserRepository;
use Blog\Repository\UserRepositoryImpl;
use Psr\Container\ContainerInterface;

return [
    'databaseConfiguration' => [
        'host'     => '127.0.0.1',
        'name'     => 'Blog',
        'username' => 'root',
        'password' => ''
    ],


    /**
     * Controllers
     */
    'controllerMapping'          => [
        '/'                 => IndexController::class,
        '/test'             => TestController::class,
        '/register'         => RegisterController::class,
        '/login'            => LoginController::class,
        '/logout'           => LogoutController::class,
        '/history/edit/{id}'     => HistoryController::class,

        /** REST controllers */
        '/api/history'      => HistoryRestController::class,
        '/api/history/{id}' => HistoryRestController::class
    ],


    /**
     * DI block
     */
    UserRepository::class        => static function (ContainerInterface $c) {
        return new UserRepositoryImpl($c->get(DataBaseConnection::class));
    },
    HistoryRepository::class     => static function (ContainerInterface $c) {
        return new HistoryRepositoryImpl($c->get(DataBaseConnection::class));
    },
    TemplateManager::class       => DI\create(TemplateManagerImpl::class),
    DataBaseConfiguration::class => static function (ContainerInterface $c) {
        return new DataBaseConfiguration($c->get('databaseConfiguration'));
    },
    DataBaseConnection::class    => static function (ContainerInterface $c) {
        return new DataBaseConnectionPdo($c->get(DataBaseConfiguration::class));
    },
];