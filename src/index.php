<?php


require '../vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');


session_start();


$containerBuilder = new DI\ContainerBuilder();

$containerBuilder->addDefinitions(require 'configuration/di.php');

$container = $containerBuilder->build();


new \Blog\Application($container);


