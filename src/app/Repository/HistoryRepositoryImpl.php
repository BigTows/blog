<?php


namespace Blog\Repository;


use Blog\Component\DataBase\DataBaseConnection;
use Blog\Domain\History;
use Blog\Domain\User;

final class HistoryRepositoryImpl implements HistoryRepository
{


    /**
     * DataBase connection
     *
     * @var DataBaseConnection
     */
    private DataBaseConnection $dataBaseConnection;

    public function __construct(DataBaseConnection $dataBaseConnection)
    {
        $this->dataBaseConnection = $dataBaseConnection;
    }

    public function findAll(int $page, int $count): array
    {
        $offset = $count * ($page - 1);
        $result = [];
        $this->dataBaseConnection->executeIterable(
            "SELECT history.id as historyId, id_user, text, user.username as username FROM history INNER JOIN user ON user.id =  history.id_user ORDER BY history.id DESC LIMIT $offset, $count",
            [],
            function ($row) use (&$result) {
                $result[] = new History($row['historyId'],
                    new User($row['id_user'], $row['username'], '', false),
                    $row['text']
                );
            }
        );
        return $result;
    }

    public function findById(int $id): History
    {
        $result = null;
        $this->dataBaseConnection->executeIterable(
            "SELECT history.id as historyId, id_user, text, user.username as username FROM history INNER JOIN user ON user.id =  history.id_user WHERE history.id  = $id",
            [
            ],
            function ($row) use (&$result) {
                $result = new History($row['historyId'],
                    new User($row['id_user'], $row['username'], '', false),
                    $row['text']
                );
            }
        );
        if (null === $result) {
            throw new Exception\NotFoundDataException("Not found history with ID $id");
        }
        return $result;
    }

    /**
     * Save history
     *
     * @param History $history
     */
    public function save(History $history): void
    {
        if ($history->getId() === null) {
            $this->dataBaseConnection->execute(
                'INSERT INTO history VALUES(NULL, ?, ?)',
                [$history->getUser()->getId(), $history->getText()]
            );
            $history->setId($this->dataBaseConnection->getLastInsertedId());
        } else {
            $this->dataBaseConnection->execute(
                'UPDATE history SET text = ? WHERE id = ?',
                [
                    $history->getText(),
                    $history->getId()
                ]
            );
        }
    }

    public function delete(History $history): void
    {
        $this->dataBaseConnection->execute(
            'DELETE FROM history WHERE id = ?',
            [
                $history->getId()
            ]
        );
    }
}