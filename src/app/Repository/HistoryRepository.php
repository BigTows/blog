<?php


namespace Blog\Repository;


use Blog\Domain\History;

/**
 * HistoryRepository
 *
 * @package Blog\Repository
 */
interface HistoryRepository
{


    /**
     * Find history by pageable
     *
     * @param int $page  number of page
     * @param int $count count of history on page
     *
     * @return History[]
     */
    public function findAll(int $page, int $count): array;


    /**
     * @param int $id
     *
     * @return History
     */
    public function findById(int $id):History;

    /**
     * Save history into database
     *
     * @param History $history
     */
    public function save(History $history): void;


    public function delete(History $history):void;

}