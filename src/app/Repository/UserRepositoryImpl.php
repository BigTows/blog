<?php


namespace Blog\Repository;


use Blog\Component\DataBase\DataBaseConnection;
use Blog\Domain\User;

final class UserRepositoryImpl implements UserRepository
{

    /**
     * Component for connection
     *
     * @var DataBaseConnection
     */
    private DataBaseConnection $dataBaseConnection;

    public function __construct(DataBaseConnection $dataBaseConnection)
    {
        $this->dataBaseConnection = $dataBaseConnection;
    }


    /**
     * @inheritDoc
     */
    public function findUserByName(string $userName): User
    {
        $userData = null;
        $this->dataBaseConnection->executeIterable('SELECT id, username, hashPassword, isAdmin FROM user WHERE username = ?', [$userName], function ($row) use (&$userData) {
            $userData = new User((int)$row['id'], $row['username'], $row['hashPassword'], (bool)$row['isAdmin']);
        });
        if (null === $userData) {
            throw new Exception\NotFoundDataException("Can't find user by username $userName");
        }
        return $userData;
    }

    public function findUserByNameAndHashPassword(string $userName, string $hashPassword): User
    {
        $userData = null;
        $this->dataBaseConnection->executeIterable('SELECT id, username, hashPassword, isAdmin FROM user WHERE username = ? AND hashPassword = ?',
            [
                $userName,
                $hashPassword
            ],
            function ($row) use (&$userData) {
                $userData = new User((int)$row['id'], $row['username'], $row['hashPassword'], (bool)$row['isAdmin']);
            });
        if (null === $userData) {
            throw new Exception\NotFoundDataException("Can't find user by username $userName");
        }
        return $userData;
    }


    public function findUserById(int $userId)
    {
        $userData = null;
        $this->dataBaseConnection->executeIterable('SELECT id, username, hashPassword, isAdmin FROM user WHERE id = ?', [$userId], function ($row) use (&$userData) {
            $userData = new User((int)$row['id'], $row['username'], $row['hashPassword'], (bool)$row['isAdmin']);
        });
        if (null === $userData) {
            throw new Exception\NotFoundDataException("Can't find user by id $userId");
        }
        return $userData;
    }


    public function saveUser(User $user)
    {
        $this->dataBaseConnection->execute(
            'INSERT INTO user VALUES (NULL, ?, ?, ?)',
            [
                $user->getName(),
                $user->getHashPassword(),
                $user->isAdmin() ? 1 : 0
            ]
        );
    }
}