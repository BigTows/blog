<?php


namespace Blog\Repository\Exception;


use RuntimeException;

/**
 * Class NotFoundDataException, when can't find entity
 *
 * @package Blog\Repository\Exception
 */
final class NotFoundDataException extends RuntimeException
{

}