<?php


namespace Blog\Repository;


use Blog\Domain\User;
use Blog\Repository\Exception\NotFoundDataException;

interface UserRepository
{


    /**
     * Find user by username
     *
     * @param string $userName
     *
     * @return User
     * @throws NotFoundDataException when can't find entity
     */
    public function findUserByName(string $userName): User;


    /**
     * Find user by name and hash password
     *
     * @param string $userName
     * @param string $hashPassword
     *
     * @return User
     * @throws NotFoundDataException when can't find entity
     */
    public function findUserByNameAndHashPassword(string $userName, string $hashPassword): User;


    public function findUserById(int $userId);


    public function saveUser(User $user);
}