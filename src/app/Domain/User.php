<?php


namespace Blog\Domain;


/**
 * User domain object
 *
 * @package Blog\Domain
 */
final class User
{
    /**
     * User id
     *
     * @var int|null
     */
    private ?int $id;

    /**
     * User nickname or login
     *
     * @var string
     */
    private string $name;

    /**
     * Hash password
     *
     * @var string
     */
    private string $hashPassword;

    /**
     * Status is admin
     *
     * @var bool
     */
    private bool $isAdmin;


    public function __construct(?int $id, string $name, string $hashPassword, bool $isAdmin)
    {
        $this->id = $id;
        $this->name = $name;
        $this->hashPassword = $hashPassword;
        $this->isAdmin = $isAdmin;
    }

    /**
     * Get user id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get user name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getHashPassword(): string
    {
        return $this->hashPassword;
    }


    /**
     * Установить
     *
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $hashPassword
     */
    public function setHashPassword(string $hashPassword): void
    {
        $this->hashPassword = $hashPassword;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }


}