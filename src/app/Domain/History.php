<?php


namespace Blog\Domain;


final class History implements \JsonSerializable
{


    /**
     * Id of history
     *
     * @var null|int
     */
    private ?int $id;

    /**
     * User
     *
     * @var User
     */
    private User $user;

    /**
     * Text of history
     *
     * @var string
     */
    private string $text;

    /**
     * History constructor.
     *
     * @param ?int   $id
     * @param User   $user
     * @param string $text
     */
    public function __construct(?int $id, User $user, string $text)
    {
        $this->id = $id;
        $this->user = $user;
        $this->text = $text;
    }

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function jsonSerialize()
    {
        return [
            'id'   => $this->getId(),
            'text' => $this->getText(),
            'user' => $this->user->getName()
        ];
    }
}