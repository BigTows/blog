<?php


namespace Blog\Controller;


use Blog\Controller\Security\SecurityContext;

interface Controller
{

    public function processRequest(SecurityContext $securityContext, string $request, array $params);




}