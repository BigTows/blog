<?php


namespace Blog\Controller\Security;


use Blog\Domain\User;
use Blog\Repository\UserRepository;
use Throwable;

final class SecurityContext
{


    /**
     * User
     *
     * @var null|User
     */
    private ?User $user = null;

    /**
     * Repository of users
     *
     * @var UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        if (isset($_SESSION['userId']) && is_int($_SESSION['userId'])) {
            $this->initializeUser($_SESSION['userId']);
        }
    }

    private function initializeUser(int $userId)
    {
        try {
            $this->user = $this->userRepository->findUserById($userId);
        } catch (Throwable $e) {

        }
    }


    public function isAnonymous(): bool
    {
        return null === $this->user;
    }


    public function authorizeUser(int $userId): void
    {
        $this->initializeUser($userId);
        $_SESSION['userId'] = $userId;
    }

    /**
     * Get current user
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }
}