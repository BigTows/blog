<?php


namespace Blog\Controller;


use Blog\Component\TemplateManager\TemplateManager;
use Blog\Controller\Security\SecurityContext;
use Blog\Repository\Exception\NotFoundDataException;
use Blog\Repository\HistoryRepository;

final class HistoryController extends PageController
{

    /**
     * History repository
     *
     * @var HistoryRepository
     */
    private HistoryRepository $historyRepository;

    public function __construct(HistoryRepository $historyRepository, TemplateManager $templateManager)
    {
        $this->historyRepository = $historyRepository;
        parent::__construct($templateManager);
    }

    protected function get(SecurityContext $securityContext, array $params): void
    {
        $this->getTemplateManager()->setView('editHistory');

        if (!isset($params['id'])) {
            $this->getTemplateManager()->addData('errorMessage', 'ID - записи нету :(');
        } else if (!$securityContext->isAnonymous() && $securityContext->getUser() !== null) {
            $this->getTemplateManager()->addData('user', $securityContext->getUser());

            try {
                $history = $this->historyRepository->findById($params['id']);
                if ($securityContext->getUser()->isAdmin() || $history->getUser()->getId() === $securityContext->getUser()->getId()) {
                    $this->getTemplateManager()->addData('history', $history);
                } else {
                    $this->getTemplateManager()->addData('errorMessage', 'Запись не доступна');
                }
            } catch (NotFoundDataException $e) {
                $this->getTemplateManager()->addData('errorMessage', 'Запись не доступна');
            }

        } else {
            $this->getTemplateManager()->addData('errorMessage', 'Доступ закрыт!');
        }
        $this->getTemplateManager()->display();
    }
}