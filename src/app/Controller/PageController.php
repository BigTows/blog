<?php


namespace Blog\Controller;


use Blog\Component\TemplateManager\TemplateManager;
use Blog\Controller\Security\SecurityContext;

abstract class PageController implements Controller
{

    /**
     * Template manager
     *
     * @var TemplateManager
     */
    private TemplateManager $templateManager;

    public function __construct(TemplateManager $templateManager)
    {
        $this->templateManager = $templateManager;
    }


    public function processRequest(SecurityContext $securityContext, string $request, array $params)
    {
        if ($request !== 'GET') {
            echo "Can't  process not GET request";
        } else {
            $this->get($securityContext, $params);
        }
    }

    abstract protected function get(SecurityContext $securityContext, array $params):void;


    /**
     * Get template component
     *
     * @return TemplateManager
     */
    protected function getTemplateManager(): TemplateManager
    {
        return $this->templateManager;
    }
}