<?php


namespace Blog\Controller;


use Blog\Component\TemplateManager\TemplateManager;
use Blog\Controller\Security\SecurityContext;
use Blog\Repository\Exception\NotFoundDataException;
use Blog\Repository\UserRepository;

/**
 * Class LoginController
 *
 * @package Blog\Controller
 */
final class LoginController implements Controller
{

    /**
     *
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var TemplateManager
     */
    private TemplateManager $templateManager;

    public function __construct(TemplateManager $templateManager, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->templateManager = $templateManager;
    }

    public function processRequest(SecurityContext $securityContext, string $request, array $params)
    {
        if ($request !== 'POST') {
            $this->templateManager->setView('errorPage');
            $this->templateManager->addData('errorMessage', 'Запрос некорректный');
            $this->templateManager->display();
        } else {
            $this->processPostRequest($params);
        }
    }

    private function processPostRequest(array $params)
    {
        if (!isset($params['username'], $params['password'])) {
            echo "Not all specified data";
            return;
        }
        try {
            $user = $this->userRepository->findUserByNameAndHashPassword($params['username'], hash('sha256', $params['password']));
            $_SESSION['userId'] = $user->getId();
            header("Location: /");
            exit(0);
        } catch (NotFoundDataException $e) {
            $this->templateManager->setView('errorPage');
            $this->templateManager->addData('errorMessage', 'Данные пользователя некорректны');
        }
        $this->templateManager->display();
    }
}