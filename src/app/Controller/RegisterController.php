<?php


namespace Blog\Controller;


use Blog\Component\TemplateManager\TemplateManager;
use Blog\Domain\User;
use Blog\Repository\Exception\NotFoundDataException;
use Blog\Repository\UserRepository;

final class RegisterController implements Controller
{


    /**
     * User repository
     *
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * Component for view template
     *
     * @var TemplateManager
     */
    private TemplateManager $templateManager;

    public function __construct(TemplateManager $templateManager, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->templateManager = $templateManager;
    }

    public function processRequest(\Blog\Controller\Security\SecurityContext $securityContext, string $request, array $params)
    {
        if ($request !== 'POST') {
            $this->templateManager->setView('errorPage');
            $this->templateManager->addData('errorMessage', 'Запрос некорректный');
            $this->templateManager->display();
        } else {
            $this->processPostRequest($params);
        }
    }

    private function processPostRequest(array $params)
    {
        if (!isset($params['username'], $params['password'])) {
            echo "Not all specified data";
            return;
        }


        try {
            $this->userRepository->findUserByName($params['username']);
            $this->templateManager->setView('errorPage');
            $this->templateManager->addData('errorMessage', 'Имя пользователя уже занято');
        } catch (NotFoundDataException $e) {

            try {
                $user = new User(null, $params['username'], hash('sha256', $params['password']), false);
                $this->userRepository->saveUser($user);
                $_SESSION['userId'] = $this->userRepository->findUserByName($user->getName())->getId();
                header("Location: /");
                exit(0);
            } catch (\Throwable $e) {
                $this->templateManager->setView('errorPage');
                $this->templateManager->addData('errorMessage', 'Error');
            }
        }
        $this->templateManager->display();

    }
}