<?php


namespace Blog\Controller;


use Blog\Component\TemplateManager\TemplateManager;
use Blog\Controller\Security\SecurityContext;

final class IndexController implements Controller
{
    /**
     * @var TemplateManager
     */
    private TemplateManager $templateManager;

    public function __construct(TemplateManager $manager)
    {
        $this->templateManager = $manager;
    }


    public function processRequest(SecurityContext $securityContext, string $request, array $params)
    {
        if ($request !== 'GET') {
            echo "Unsupported method";
        } else {
            $this->view($securityContext);
        }
    }

    private function view(SecurityContext $securityContext)
    {
        $this->templateManager->setView('index');
        if (!$securityContext->isAnonymous()) {
            $this->templateManager->addData('user', $securityContext->getUser());
        }

        $this->templateManager->display();
    }
}