<?php


namespace Blog\Controller;


use Blog\Controller\Security\SecurityContext;

final class LogoutController implements Controller
{

    public function processRequest(SecurityContext $securityContext, string $request, array $params): void
    {
        unset($_SESSION['userId']);
        header("Location: /");
    }
}