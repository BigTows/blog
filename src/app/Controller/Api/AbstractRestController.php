<?php


namespace Blog\Controller\Api;


use Blog\Controller\Controller;
use Blog\Controller\Security\SecurityContext;

abstract class AbstractRestController implements Controller
{

    final public function processRequest(SecurityContext $securityContext, string $request, array $params)
    {
        header('Content-type: application/json');
        $result = $this->buildResultByRequestType($securityContext, $request, $params);
        echo json_encode($result, JSON_THROW_ON_ERROR);
    }

    private function buildResultByRequestType(SecurityContext $securityContext, string $request, array $params)
    {
        switch ($request) {
            case 'GET':
            {
                return $this->get($securityContext, $params);
            }
            case 'POST':
            {
                return $this->post($securityContext, $params);
            }

            case 'PUT':
            {
                return $this->put($securityContext, $params);
            }
            case 'DELETE':
            {
                return $this->delete($securityContext, $params);
            }
            default:
                http_response_code(405);
                return [];
        }
    }


    protected function get(SecurityContext $securityContext, array $params): array
    {
        http_response_code(405);
        return [];
    }

    protected function post(SecurityContext $securityContext, array $params)
    {
        http_response_code(405);
        return [];
    }

    protected function put(SecurityContext $securityContext, array $params)
    {
        http_response_code(405);
        return [];
    }

    protected function delete(SecurityContext $securityContext, array $params)
    {
        http_response_code(405);
        return [];
    }
}