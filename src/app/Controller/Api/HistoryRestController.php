<?php


namespace Blog\Controller\Api;


use Blog\Controller\Security\SecurityContext;
use Blog\Domain\History;
use Blog\Repository\Exception\NotFoundDataException;
use Blog\Repository\HistoryRepository;

final class HistoryRestController extends AbstractRestController
{

    /**
     * History repository
     *
     * @var HistoryRepository
     */
    private HistoryRepository $historyRepository;

    public function __construct(HistoryRepository $historyRepository)
    {
        $this->historyRepository = $historyRepository;
    }


    protected function get(SecurityContext $securityContext, array $params): array
    {

        return $this->historyRepository->findAll(1, 100);
    }

    protected function post(SecurityContext $securityContext, array $params)
    {

        if ($securityContext->isAnonymous()) {
            http_response_code(403);
            return [
                'Forbidden' => 'Please authorize'
            ];
        }


        if (!isset($params['text'])) {
            return [
                'Error' => 'Text not present'
            ];
        }


        $history = new History(null, $securityContext->getUser(), $params['text']);

        $this->historyRepository->save($history);
        return [$history];
    }


    protected function delete(SecurityContext $securityContext, array $params)
    {
        if ($securityContext->isAnonymous() || $securityContext->getUser() === null) {
            http_response_code(403);
            return [
                'Forbidden' => 'Please authorize'
            ];
        }
        if (!isset($params['id'])) {
            http_response_code(400);
            return [
                'Error' => 'Id not present'
            ];
        }

        $history = $this->historyRepository->findById((int)$params['id']);

        if ($securityContext->getUser()->isAdmin() || $history->getUser()->getId() === $securityContext->getUser()->getId()) {
            $this->historyRepository->delete($history);
        } else {
            http_response_code(403);
        }

        return [];
    }

    protected function put(SecurityContext $securityContext, array $params)
    {
        if ($securityContext->isAnonymous() || $securityContext->getUser() === null) {
            http_response_code(403);
            return [
                'Forbidden' => 'Please authorize'
            ];
        }

        //TODO move to Application layer
        parse_str(file_get_contents("php://input"), $data);
        if (!isset($data['text'], $params['id'])) {
            http_response_code(400);
            return [
                'Error' => 'Id or text not present'
            ];
        }

        try {
            $history = $this->historyRepository->findById((int)$params['id']);
            if ($securityContext->getUser()->isAdmin() || $history->getUser()->getId() === $securityContext->getUser()->getId()) {
                $history->setText($data['text']);
                $this->historyRepository->save($history);
            }
        } catch (NotFoundDataException $e) {
            http_response_code(404);
        }

        return [];
    }
}