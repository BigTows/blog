<?php


namespace Blog\Component\TemplateManager;

/**
 * Simple template manager
 *
 * @package Blog\Component\TemplateManager
 */
final class TemplateManagerImpl implements TemplateManager
{
    /**
     * Path for dir with template
     */
    private const BASE_PATH_TEMPLATE = __DIR__ . '/../../../template/';

    /**
     * Name of template (view)
     *
     * @var string
     */
    private string $viewOfView;

    /**
     * Data of template
     *
     * @var array
     */
    private array $data = [];

    /**
     * @inheritDoc
     */
    public function setView(string $nameOfView): void
    {
        $this->viewOfView = $nameOfView;
    }

    /**
     * @inheritDoc
     */
    public function addData(string $nameData, $data): void
    {
        $this->data[$nameData] = $data;
    }

    /**
     * @inheritDoc
     */
    public function display(): void
    {
        foreach ($this->data as $nameVar => $value) {
            ${$nameVar} = $value;
        }

        /** @noinspection PhpIncludeInspection */
        require_once self::BASE_PATH_TEMPLATE . $this->viewOfView . '.phtml';
    }
}