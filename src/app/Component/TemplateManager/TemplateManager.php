<?php


namespace Blog\Component\TemplateManager;


/**
 * Interface TemplateManager, for display content
 *
 * @package Blog\Component\TemplateManager
 */
interface TemplateManager
{
    /**
     * Set name of template (view)
     *
     * @param string $nameOfView
     */
    public function setView(string $nameOfView): void;

    /**
     * Added data for template
     *
     * @param string $nameData
     * @param mixed  $data
     *
     * @return void
     */
    public function addData(string $nameData, $data): void;


    /**
     * Display content
     *
     * @return void
     */
    public function display(): void;
}