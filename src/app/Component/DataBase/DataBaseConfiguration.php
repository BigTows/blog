<?php


namespace Blog\Component\DataBase;

/**
 * Configuration of database connection
 *
 * @package Blog\DataBase
 */
final class DataBaseConfiguration
{
    /**
     * Host at database
     *
     * @var string
     */
    private $host;
    /**
     * Database name
     *
     * @var string
     */
    private $name;
    /**
     * Username at database
     *
     * @var string
     */
    private $username;
    /**
     * Password at database
     *
     * @var string
     */
    private $password;

    public function __construct(array $databaseConnectionConfiguration)
    {
        $this->validate($databaseConnectionConfiguration);

        $this->host = $databaseConnectionConfiguration['host'];
        $this->name = $databaseConnectionConfiguration['name'];
        $this->username = $databaseConnectionConfiguration['username'];
        $this->password = $databaseConnectionConfiguration['password'];
    }

    private function validate(array $databaseConnectionConfiguration)
    {
        if (!isset(
            $databaseConnectionConfiguration['host'],
            $databaseConnectionConfiguration['name'],
            $databaseConnectionConfiguration['username'],
            $databaseConnectionConfiguration['password']
        )) {
            throw new \InvalidArgumentException("DataBase configuration invalid");
        }
    }

    /**
     * Get host database
     *
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Get name at database
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get user name at database
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Get password at database
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }


}