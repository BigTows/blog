<?php


namespace Blog\Component\DataBase;


interface DataBaseConnection
{

    /**
     * Execute SQL with prepared statements and iterate it
     *
     * @param string   $sql      SQL Query
     * @param array    $params   params of query
     * @param callable $iterator call every row
     *
     * @return void
     */
    public function executeIterable(string $sql, array $params, callable $iterator): void;

    /**
     * Execute SQL
     *
     * @param string $sql    SQL Query
     * @param array  $params params of query
     */
    public function execute(string $sql, array $params = []): void;

    /**
     * Get last inserted id
     *
     * @return int
     */
    public function getLastInsertedId(): int;

}