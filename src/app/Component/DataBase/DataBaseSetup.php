<?php


namespace Blog\Component\DataBase;


use RuntimeException;

/**
 * Setup database environment
 * <ul>
 * <li>Create all tables</li>
 * <li>Create all key and indexes</li>
 * </ul>
 *
 * @package Blog\Component\DataBase
 */
final class DataBaseSetup
{
    /**
     * DataBaseSetup constructor.
     *
     * @param DataBaseConnection $dataBaseConnection
     */
    public function __construct(DataBaseConnection $dataBaseConnection)
    {
        $this->checkExistsUserTable($dataBaseConnection);
        $this->checkExistsHistoryTable($dataBaseConnection);
    }

    /**
     * Check user table for exists
     *
     * @param DataBaseConnection $dataBaseConnection
     */
    private function checkExistsUserTable(DataBaseConnection $dataBaseConnection): void
    {
        try {
            $dataBaseConnection->execute('SELECT 1 FROM user');
        } catch (RuntimeException $exception) {
            $dataBaseConnection->execute(
                <<<EOF
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `hashPassword` varchar(64) NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='User of blog';
EOF
            );
        }
    }

    /**
     * Check history table for exists
     *
     * @param DataBaseConnection $dataBaseConnection
     */
    private function checkExistsHistoryTable(DataBaseConnection $dataBaseConnection): void
    {
        try {
            $dataBaseConnection->execute('SELECT 1 FROM history');
        } catch (RuntimeException $exception) {
            $dataBaseConnection->execute(
                <<<EOF
CREATE TABLE `history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int DEFAULT NULL,
  `text` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `history_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
EOF
            );
        }
    }
}