<?php


namespace Blog\Component\DataBase;


use PDO;
use RuntimeException;

class DataBaseConnectionPdo implements DataBaseConnection
{
    /**
     * @var DataBaseConfiguration
     */
    private DataBaseConfiguration $configuration;

    /**
     * Connection of DataBase
     *
     * @var PDO
     */
    private PDO $connection;

    /**
     * DataBaseConnectionPdo constructor.
     *
     * @param DataBaseConfiguration $configuration
     */
    public function __construct(DataBaseConfiguration $configuration)
    {
        $this->configuration = $configuration;

        $this->connection = new PDO(
            "mysql:dbname={$configuration->getName()};host={$configuration->getHost()}",
            $configuration->getUsername(),
            $configuration->getPassword(),
            [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function executeIterable(string $sql, array $params, callable $iterator): void
    {
        $statement = $this->createStatementAndExecute($sql, $params);
        while ($row = $statement->fetch()) {
            $iterator($row);
        }
    }

    /**
     * @inheritDoc
     */
    public function execute(string $sql, array $params = []): void
    {
        $this->createStatementAndExecute($sql, $params);
    }

    /**
     * @param string $sql
     * @param array  $params
     *
     * @return bool|\PDOStatement
     */
    private function createStatementAndExecute(string $sql, array $params)
    {
        $statement = $this->connection->prepare($sql);
        $statement->execute($params);
        if ($statement->errorInfo()[0] !== '00000') {
            throw new RuntimeException("Can't execute query: " . print_r($params, true) . "\n" . print_r($statement->errorInfo(), true));
        }
        return $statement;
    }

    /**
     * @inheritDoc
     */
    public function getLastInsertedId(): int
    {
        return (int)$this->connection->lastInsertId();
    }


}