<?php


namespace Blog;


use Blog\Component\DataBase\DataBaseSetup;
use Blog\Controller\Controller;
use Blog\Controller\Security\SecurityContext;
use Blog\Repository\UserRepository;
use DI\Container;

/**
 * Startup application
 *
 * @package Blog
 */
final class Application
{
    private Container  $container;

    /**
     * Application constructor.
     *
     * @param Container $container
     *
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->setupEnvironment($container);
        $this->processRequest();
    }

    /**
     * Setup environment
     *
     * @param Container $container
     *
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    private function setupEnvironment(Container $container): void
    {
        $container->get(DataBaseSetup::class);
    }


    private function processRequest(): void
    {
        $requestMethod = $_SERVER['REQUEST_METHOD'];
        $pathServer = $_SERVER['REQUEST_URI'];
        $security = new SecurityContext($this->container->get(UserRepository::class));
        foreach ($this->container->get('controllerMapping') as $path => $value) {
            if ($path === $_SERVER['REQUEST_URI']) {
                $controller = $this->container->get($value);
                if (!($controller instanceof Controller)) {
                    throw new \RuntimeException("Can't create controller for path: $path");
                }

                $controller->processRequest($security, $requestMethod, $_REQUEST);
                return;
            } else {
                $serverParsed = explode('/', $pathServer);
                $controllerParser = explode('/', $path);
                if (count($serverParsed) === count($controllerParser)) {
                    $params = [];
                    $isFound = true;
                    for ($i = 0, $iMax = count($serverParsed); $i < $iMax; $i++) {

                        if ($serverParsed[$i] !== $controllerParser[$i]) {
                            if (strpos($controllerParser[$i], '{') === 0) {
                                $valueParam = str_replace(['{', '}'], '', $controllerParser[$i]);
                                $params[$valueParam] = $serverParsed[$i];
                            } else {
                                $isFound = false;
                                break;
                            }
                        }
                    }
                    if ($isFound) {
                        $controller = $this->container->get($value);
                        if (!($controller instanceof Controller)) {
                            throw new \RuntimeException("Can't create controller for path: $path");
                        }
                        $controller->processRequest($security, $requestMethod, $params);
                        return;
                    }
                }
            }
        }

        http_response_code(404);
    }
}